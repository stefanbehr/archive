$(document).ready(function(){

	/* add toggle buttons to first li elements of all lists except the most deeply nested ones */
	$(".course > ul").children().each(function(){
		var button_hide = "<span class='toggle_list'>&minus;</span>";
		$(this).prepend(button_hide);
		$(this).children().has("li ul").children().prepend(button_hide);
	});

	/* add list-hiding toggle functionality to list toggle buttons, modify buttons according to state */
	$(".toggle_list").toggle(function(){
		$(this).parent().children(":not(span)").hide();
		$(this).html("&#43;");
	}, function(){
		$(this).parent().children().show();
		$(this).html("&minus;");
	});
	
	/* add list-styling to all list items that don't have toggle buttons */
	$("li:not(:has(.toggle_list))").css("list-style-type", "circle");
	
	/* puts some placeholder text in list items that are empty */
	$("li:empty").append("[no items yet]");

	/* add toggle functionality to individual course listings */
	$(".course .course_name").click(function(){
		$(this).next().slideToggle('fast');
	});
	
	/* create button for toggling all course listings on or off */
	$("#toggle_all").toggle(function(){
		$(".course > ul").slideUp('fast', function(){
			$("#toggle_all h1").text("show all courses");
		});
	}, function(){
		$(".course > ul").slideDown('fast', function(){
			$("#toggle_all h1").text("hide all courses");
		});
	});
});